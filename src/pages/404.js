import React from "react"
import { GoHome } from "."

const NotFoundPage = ({}) => {
  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
      }}
    >
      <img alt="Not Found" src="/404.png" />
      <GoHome />
    </div>
  )
}

export default NotFoundPage
