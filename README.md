

> https://lapassion.tech/ <

## Comment ajouter une citation ?

Par exemple tu souhaites ajouter la citation `Quoi tu es passioné.e et tu es Gold à LoL`
créé le fichier suivant `tu_es_gold_a_lol.md` dans `./src/quoites/`
```
---
slug: "/quoites/tu_es_gold_a_lol"
date: "2020-11-07"
title: "tu es Gold à LoL"
---
```


### Static images from :
* (CC BY 3.0) heart -> https://www.iconfinder.com/icons/4096575/heart_like_love_icon
